<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserMysql;

class TestMysqlDriver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mysql';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test searching with scout mysql driver';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = UserMysql::all()->count();
        $this->comment("UserMysql records count: {$count}");
        $term = 'David';
        $this->comment("Searching for User with keyword: {$term}");

        $results = UserMysql::search($term)->get();

        $modes = [
            'NATURAL_LANGUAGE',
            'BOOLEAN',
            'LIKE',
            'LIKE_EXPANDED',
        ];

        $this->comment("Driver: MySQL");
        foreach ($modes as $mode) {
            config(['scout.mysql.mode' => $mode]);
            config(['scout.mysql.query_expansion' => false]);

            $this->comment("Mode: {$mode}");
            $start = now();
            $results = UserMysql::search($term)->get();
            $time = $start->diffInMilliseconds(now());

            $this->line("Found {$results->count()} record(s).");
            /*
            foreach ($results as $key => $result) {
                $no = $key + 1;
                $this->line("{$no}. {$result->name} - {$result->email}");
            }
             */
            $this->info("Execution time: {$time} ms");

            config(['scout.mysql.query_expansion' => true]);
            $this->comment("Mode: {$mode} with Query Expansion");
            $start = now();
            $results = UserMysql::search($term)->get();
            $time = $start->diffInMilliseconds(now());

            $this->line("Found {$results->count()} record(s).");
            /*
            foreach ($results as $key => $result) {
                $no = $key + 1;
                $this->line("{$no}. {$result->name} - {$result->email}");
            }
             */
            $this->info("Execution time: {$time} ms");

        }
    }
}
