<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Film extends Model
{
    use Searchable;

	/**
	 * The primary key for the model
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	protected $fillable = ['title'];

	/**
	 * Get the index name for the model
	 *
	 * @return string
	 */
	public function searchableAs()
	{
		return 'film_index';
	}
}
