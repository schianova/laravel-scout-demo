<?php

namespace App;

class UserMysql extends User
{
    public function searchableAs()
    {
        return 'users_mysql_index';
    }
}
