<?php

use Illuminate\Database\Seeder;
use App\UserMysql;

class UserMysqlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserMysql::class, 10000)->create();
    }
}
